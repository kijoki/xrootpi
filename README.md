# README #

This repository is created for basic implementation for 
1. TCP Server
2. WebSocket Server
3. Async Serial Library
4. WebSocket Conversion for TCP and Serial Devices


### What is this repository for? ###

* This is a personal project to create libraries for further development work 

### How do I get set up? ###

You will require the following:
* VS2013
* Boost C++ Lib