#include "SKLib.h"

char* SKLib::ToCharArray(std::string key)
{
	char *a = new char[key.size() + 1];
	a[key.size()] = 0;
	memcpy(a, key.c_str(), key.size());
	return a;
}
int SKLib::EncodeBase64(const char *in, int in_len, char *out, int out_size)
{

	static const char encode[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz0123456789+/";
	unsigned char triple[3];
	int i;
	int len;
	int line = 0;
	int done = 0;

	while (in_len) {
		len = 0;
		for (i = 0; i < 3; i++) {
			if (in_len) {
				triple[i] = *in++;
				len++;
				in_len--;
			}
			else
				triple[i] = 0;
		}
		if (len) {

			if (done + 4 >= out_size)
				return -1;

			*out++ = encode[triple[0] >> 2];
			*out++ = encode[((triple[0] & 0x03) << 4) |
				((triple[1] & 0xf0) >> 4)];
			*out++ = (len > 1 ? encode[((triple[1] & 0x0f) << 2) |
				((triple[2] & 0xc0) >> 6)] : '=');
			*out++ = (len > 2 ? encode[triple[2] & 0x3f] : '=');

			done += 4;
			line += 4;
		}
		if (line >= 72) {

			if (done + 2 >= out_size)
				return -1;

			*out++ = '\r';
			*out++ = '\n';
			done += 2;
			line = 0;
		}
	}

	if (done + 1 >= out_size)
		return -1;

	*out++ = '\0';

	return done;
}