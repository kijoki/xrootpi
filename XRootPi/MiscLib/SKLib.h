#ifndef SKLIB_H
#define SKLIB_H

#include <string>
#include <cstring>
class SKLib
{
public:
	static int EncodeBase64(const char *in, int in_len, char *out, int out_size);
	static char* ToCharArray(std::string key);
};

#endif