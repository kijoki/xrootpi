#include "TCPClient.h"

/*
TCPClient::TCPClient(boost::asio::io_service& ioService) :
	m_IoService(ioService),
	socket(ioService)
{
	TCPClient::Connect("127.0.0.1", 65);
}
*/

TCPClient::TCPClient(char *ip, int port, boost::asio::io_service& ioService, MessageFactory* msgFactory) :
m_IoService(ioService)
{
	m_factory = msgFactory;
	msgFactory->MsgSend.connect(boost::bind(&TCPClient::Write, this, _1));
	new_connection = new Connection(m_IoService);
	new_connection->msgFactory = m_factory;
	TCPClient::Connect(ip, port);
}


TCPClient::~TCPClient()
{
}



void TCPClient::Connect(char *ip, int port)
{
	boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::address::from_string(ip), port);
	
	new_connection->m_ClientSocket.async_connect(endpoint,
		boost::bind(&TCPClient::Handle_Connect, this,
		boost::asio::placeholders::error));

	
}

void TCPClient::Handle_Connect(const boost::system::error_code& err)
{
	new_connection->ReadSome();
}

void TCPClient::Write(unsigned char *s)
{
	new_connection->write(s);
}