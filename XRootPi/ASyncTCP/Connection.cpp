#include "Connection.h"

Connection::Connection(boost::asio::io_service& ioService) :
m_IoService(ioService),
m_ClientSocket(ioService)
{

}

Connection::~Connection()
{
	msgFactory->~MessageFactory();
}

boost::asio::ip::tcp::socket* Connection::CreateSocket()
{
	
	return &m_ClientSocket;
}

/*TCPServer::ReadFromClient()
* Note: Event handler to read from client asynchronously
*/
void Connection::ReadSome()
{

	//boost::system::error_code ec;

	m_ClientSocket.async_read_some(boost::asio::buffer(m_Buffer, sizeof(m_Buffer)),
		boost::bind(&Connection::ReadFromClient, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
	
}

/* TCPServer::ReadSome(const boost::system::error_code& error, size_t bytes_transferred)
* @param 1: error code
* @param 2: number of bytes transferred
* Note: Send data from TCPServer
*/
void Connection::ReadFromClient(const boost::system::error_code& error, size_t bytes_transferred)
{

	if (!error) 
	{
		m_Buffer[bytes_transferred] = 0;
		msgFactory->ReadMessage(m_Buffer);
		Connection::ReadSome();
		std::cout << "Connection::ReadFromClient: Messsage Read\n";

		
	}
	else
	{
		std::cout << "Closing Connection\n";
		m_ClientSocket.close();
	//	StartAccept();
	}
}

/* void TCPServer::SendToClient(const boost::system::error_code& error, size_t bytes_transferred)
* @param 1: Boost system error code
* @param 2: Bytes transferred
* Note:
*/
void Connection::SendToClient(const boost::system::error_code& error, size_t bytes_transferred)
{

	if (!error)
	{
		std::cout << "Connection::SendToClient: Messsage Sent\n";
	}
	else {
		std::cout << "Messsage not sent " << error.message() << std::endl;
	}

}

void Connection::Close()
{
	m_ClientSocket.close();
}

void Connection::write(unsigned char *msg)
{
	int sizestr = std::strlen((char*)msg);
	std::cout << "Connection::write: " <<msg <<"\n";
	m_ClientSocket.async_write_some(boost::asio::buffer(msg, sizestr), boost::bind(&Connection::SendToClient,
		this,
		boost::asio::placeholders::error,
		boost::asio::placeholders::bytes_transferred));
}