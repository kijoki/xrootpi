#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <unordered_map>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include "Connection.h"
class TCPClient
{
public:
//	TCPClient(boost::asio::io_service& ioService);
	TCPClient(char *ip, int port, boost::asio::io_service& ioService, MessageFactory* msgFactory);
	boost::asio::io_service& m_IoService;
	~TCPClient();
	//boost::asio::ip::tcp::socket socket;
	void Connect(char *ip, int port);
	void Handle_Connect(const boost::system::error_code& err);
	MessageFactory* m_factory;
	void Write(unsigned char *s);
	Connection *new_connection;
};

#endif
