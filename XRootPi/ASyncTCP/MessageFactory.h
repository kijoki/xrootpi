#ifndef MESSAGEFACTORY_H
#define MESSAGEFACTORY_H

#include <iostream>
#include <boost/signals/signal1.hpp>
class MessageFactory
{
public:
	MessageFactory();
	~MessageFactory();
	void ReadMessage(unsigned char *s);
	void WriteMessage(unsigned char *s);
	unsigned char* Read();
	boost::signal1<void, unsigned char*> MsgRecv;
	boost::signal1<void, unsigned char*> MsgSend;
};

#endif
