#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <unordered_map>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include "Connection.h"
#include <unordered_map>
class TCPServer
{
public:
	//TCPServer(boost::asio::io_service& ioService);
	TCPServer(char *ip, int port, boost::asio::io_service& ioService, MessageFactory* msgFactory);
	void InitAcceptor(char *ip, int port);
	void StartAccept();
	void OnSocketAccept(const boost::system::error_code& error, Connection *handle);
	void Run();
	~TCPServer();
	std::hash<Connection*> ConnectionHash;
	std::unordered_map<size_t, Connection* >conn_list;
	boost::asio::ip::tcp::acceptor m_ClientAcceptor;
	boost::asio::io_service& m_IoService;
	void Broadcast(unsigned char* s);
	MessageFactory* m_factory;
};

#endif