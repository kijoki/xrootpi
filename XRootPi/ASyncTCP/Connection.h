#ifndef CONNECTION_H
#define CONNECTION_H

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include "MessageFactory.h"
class Connection
{
public:
	boost::asio::ip::tcp::socket m_ClientSocket;
	boost::asio::ip::tcp::socket* CreateSocket();
	Connection(boost::asio::io_service& ioService);
	void ReadFromClient(const boost::system::error_code& error, size_t bytes_transferred);
	void ReadSome();
	void SendToClient(const boost::system::error_code& error, size_t bytes_transferred);
	void write(unsigned char *msg);
	unsigned char m_Buffer[1024];
	boost::asio::io_service& m_IoService;
	MessageFactory *msgFactory;
	void Close();
	~Connection();
};

#endif