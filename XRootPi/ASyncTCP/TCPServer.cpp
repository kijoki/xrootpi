#include "TCPServer.h"
/*
TCPServer::TCPServer(boost::asio::io_service& ioService) :
m_IoService(ioService),
m_ClientAcceptor(ioService)
{
	InitAcceptor();
}
*/

TCPServer::TCPServer(char *ip, int port, boost::asio::io_service& ioService, MessageFactory* msgFactory) :
m_IoService(ioService),
m_ClientAcceptor(ioService)
{
	m_factory = msgFactory;
	msgFactory->MsgSend.connect(boost::bind(&TCPServer::Broadcast, this, _1));
	TCPServer::InitAcceptor(ip, port);
}



TCPServer::~TCPServer()
{
}

/* TCPServer::InitAcceptor()
* Note: Initiating TCPSocket
*/
void TCPServer::InitAcceptor(char *ip, int port)
{
	/* Find Ipaddress and listen there */
	boost::asio::ip::address_v4 listenAddress;
	listenAddress = boost::asio::ip::address_v4::from_string(ip);
	boost::asio::ip::tcp::endpoint endpoint(listenAddress,(unsigned short)port);
	m_ClientAcceptor.open(endpoint.protocol());
	m_ClientAcceptor.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
	m_ClientAcceptor.bind(endpoint);
	m_ClientAcceptor.listen();
	StartAccept();
}


/* TCPServer::StartAccept()
* Note: Lisitening to TCPSocket
*/
void TCPServer::StartAccept()
{

	try
	{
		Connection *new_connection = new Connection(m_IoService);
		Connection *handle = new Connection(m_IoService);
		handle = new_connection;
		handle->msgFactory = m_factory;

		m_ClientAcceptor.async_accept(handle->m_ClientSocket,
			boost::bind(&TCPServer::OnSocketAccept, this, boost::asio::placeholders::error, handle));

		new_connection->Close();
	}
	catch (boost::system::system_error& e)
	{
		std::cout <<"Error Accepting Connection";
	}
}

/* TCPServer::OnSocketAccept(const boost::system::error_code& error)
* Note: Accepting Connection
*/
void TCPServer::OnSocketAccept(const boost::system::error_code& error, Connection* handle)
{

	try
	{
		std::cout << "Accepted Connection\n";
		std::hash<Connection*> hash_fn;
		size_t hash = hash_fn(handle);
		conn_list[hash] = handle;
		handle->ReadSome();
		StartAccept();
		
		
	}
	catch (boost::system::system_error& e)
	{
		std::cout << "On Socket Error " << e.what();
	}
}

void TCPServer::Broadcast(unsigned char *s)
{

	std::cout << "TCPServer::Broadcast: " << s <<"\n";

	for (auto it = conn_list.begin(); it != conn_list.end(); ++it)
	{
		

			Connection *handle = (Connection*)it->second;
			handle->write(s);

		
	}

}
	
void TCPServer::Run()
{
	m_IoService.run();
}
