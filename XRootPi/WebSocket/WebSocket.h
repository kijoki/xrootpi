#ifndef WEBSOCKET_H
#define WEBSOCKET_H

#include "../ASyncTCP/TCPServer.h"
#include "../MiscLib/SKLib.h"
#include "WebSocketHelper.h"
#include <boost/thread.hpp>
#include <boost/uuid/sha1.hpp>
using namespace std;
class WebSocket
{
public:
	WebSocket(char *ip, int port, boost::asio::io_service& ioService, MessageFactory* msgF);
	~WebSocket();
	int WSMsgHandler(unsigned char* s);
	bool DoHandshake(unsigned char* s);
	string ReadHandshake(unsigned char* s);
	string GetHeadervalue(string data, string header);
	
	void SetChallengeResponse_Base64(string data);
	bool handShakeComplete;
	MessageFactory *msgFactory = new MessageFactory();
	unsigned char mask_key[4];
	void SendToClient(char *msgToSend);
	bool isMask;
	int ping_interval;
	boost::asio::ip::tcp::acceptor m_ClientAcceptor;
	boost::asio::io_service& m_IoService;
};

#endif