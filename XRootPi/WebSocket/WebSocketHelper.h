#include <string>
using namespace std;
class WebSocketHelper
{
public:
	WebSocketHelper();
	~WebSocketHelper();
	int ParseMaskedData(unsigned char* key, unsigned char* data, unsigned char* actual_data);
	void UnMaskData(unsigned char* key, unsigned char* data, int len, unsigned char* un_masked_data);
	void WebSocketFraming(string m_TxBuffer, unsigned char* buffer_to_send);
};

