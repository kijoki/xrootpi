#include "Serial.h"

Serial::Serial(const char *port_name, const int baud_rate, boost::asio::io_service& ioService) : m_io(ioService), m_port(m_io, port_name)
{
	m_port.set_option(boost::asio::serial_port_base::parity());	// default none
	m_port.set_option(boost::asio::serial_port_base::character_size(8));
	m_port.set_option(boost::asio::serial_port_base::stop_bits());	// default one
	m_port.set_option(boost::asio::serial_port_base::baud_rate(baud_rate));
	read_some();
	boost::bind(static_cast<size_t(boost::asio::io_service::*)()>(&boost::asio::io_service::run), &m_io);
}

Serial::~Serial() {
	// TODO Auto-generated destructor stub
}

void Serial::read_handler(const boost::system::error_code& error, size_t bytes_transferred)
{
	read_msg[bytes_transferred] = 0;
	std::cout << bytes_transferred << " bytes: " << read_msg << std::endl;

	read_some();
}

void Serial::do_write(char* msg)
{
	m_port.async_write_some(boost::asio::buffer(msg, std::strlen(msg) + 1), boost::bind(&Serial::write_handler,
		this,
		boost::asio::placeholders::error,
		boost::asio::placeholders::bytes_transferred));

}

void Serial::read_some()
{
	m_port.async_read_some(boost::asio::buffer(read_msg, 512),
		boost::bind(&Serial::read_handler,
		this,
		boost::asio::placeholders::error,
		boost::asio::placeholders::bytes_transferred));
}

void Serial::write_handler(const boost::system::error_code& error, size_t bytes_transferred)
{
	//TODO Add logging
}

void Serial::write(char *msg)
{
	m_io.post(boost::bind(&Serial::do_write, this, msg));
}

void Serial::Run()
{
	m_io.run();
}