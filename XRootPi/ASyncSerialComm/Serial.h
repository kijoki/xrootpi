#ifndef SERIAL_H
#define SERIAL_H

#include <iostream>
#include <string>
#include <boost/asio.hpp>
#include <boost/asio/serial_port.hpp>
#include <boost/system/error_code.hpp>
#include <boost/system/system_error.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>

class Serial {
	char read_msg[512];
	boost::asio::io_service& m_io;
	boost::asio::serial_port m_port;

public:
	Serial(const char *port_name, const int baud_rate, boost::asio::io_service& ioService);
	virtual ~Serial();
	void write_handler(const boost::system::error_code& error, size_t bytes_transferred);
	void write(char *msg);
	void Run();

private:
	void read_handler(const boost::system::error_code& error, size_t bytes_transferred);
	void do_write(char* msg);
	void read_some();
};

#endif