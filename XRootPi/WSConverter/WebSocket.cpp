#include "WebSocket.h"

WebSocket::WebSocket(char *ip, int port, boost::asio::io_service& ioService, MessageFactory* msgF) :
m_IoService(ioService),
m_ClientAcceptor(ioService)
{
	//boost::asio::io_service mIO;
	//handShakeComplete = false;
	isMask = false;
	ping_interval = 50;
	//TCPServer s(mIO);
	msgFactory = msgF;
	msgFactory->MsgRecv.connect(boost::bind(&WebSocket::WSMsgHandler, this, _1));
	try {
		TCPServer s(ip, port, m_IoService, msgFactory);
		boost::thread t(boost::bind(static_cast<size_t(boost::asio::io_service::*)()>(&boost::asio::io_service::run), &m_IoService));
		t.join();
		/*
		boost::thread t(boost::bind(static_cast<size_t(boost::asio::io_service::*)()>(&boost::asio::io_service::run), &m_IoService));
		char *line = new char[100];
		while (cin.getline(line, 100))
		{
			if (line[0] != '\0')
				WebSocket::SendToClient(line);
		}
		t.join();
		*/
	}
	catch (boost::system::system_error& e)
	{
		cout << "WebSocket Initialization Error\n";
	}
	
	
}

WebSocket::~WebSocket()
{
}

int WebSocket::WSMsgHandler(unsigned char* s)
{
	
	if (s[0] != 129)
	{
		WebSocket::DoHandshake(s);
		return 0;
	}


	return 0;
}


/* void WebSocket::DoHandshake()
* Note: Handshake procedure to establish connection with client
*/
bool WebSocket::DoHandshake(unsigned char* s)
{
	stringstream ss;
	ss << s;

	//get client handshake data
	string data = ss.str();

	string protocol;
	string location;
	string origin;
	protocol = GetHeadervalue(data, "Sec-WebSocket-Version: ");
	location = GetHeadervalue(data, "Host: ");
	origin = GetHeadervalue(data, "Origin: ");

	if (atoi(protocol.c_str()) >= 8)
		SetChallengeResponse_Base64(data);

	//handShakeComplete = true;
	cout << "HandShake Completed\n";
	return true;
}


/* void WebSocket::GetHeadervalue(string data, string header)
* @param 1: Data
* @param 1: Header
* Note: Parse the header value
*/
string WebSocket::GetHeadervalue(string data, string header)
{
	string value;
	size_t index = data.find(header);
	if (index != string::npos){
		value = data.substr(index + header.length()); //+2more???
		size_t end = value.find("\r\n");
		value = value.substr(0, end);
	}
	return value;
}

/* void WebSocket::SetChallengeResponse_Base64(string data)
* @param 1: Header data sent by the client
* Note: Gets the key, encodes in sha-1 than with base64 to produce accept key
*/
void WebSocket::SetChallengeResponse_Base64(string data)
{
	string key = GetHeadervalue(data, "Sec-WebSocket-Key: ");
	string protocol = GetHeadervalue(data, "Sec-WebSocket-Protocol: ");
	stringstream sss;
	sss << key;
	sss << "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";

	string val = sss.str();

	char* m_Buf2 = SKLib::ToCharArray(val);

	int lenth = strlen(m_Buf2);

	boost::uuids::detail::sha1 sha;
	sha.process_bytes(m_Buf2, 60);
	unsigned int digest[5]; //Sha1 value as int
	sha.get_digest(digest);
	int c = 0;
	//convert to char
	unsigned char hash[20];
	for (int i = 0; i<5; i++) {
		hash[c++] = (digest[i] & 0xff000000) >> 24;
		hash[c++] = (digest[i] & 0xff0000) >> 16;
		hash[c++] = (digest[i] & 0xff00) >> 8;
		hash[c++] = digest[i] & 0xff;
	}
	//-- Using base64-decode lib
	size_t len;
	len = SKLib::EncodeBase64((char *)hash, 20, m_Buf2, lenth);


	stringstream ss;
	ss << "HTTP/1.1 101 Switching Protocols\r\n";
	ss << "Upgrade: websocket\r\n";
	ss << "Connection: Upgrade\r\n";
	ss << "Sec-WebSocket-Accept: " << m_Buf2 <<"\r\n";
	ss << "Sec-WebSocket-Protocol: "<< protocol;
	ss << "\r\n\r\n";
	//ss << '\0';
	string g = ss.str();
	
	unsigned char* handShakeMsg = (unsigned char*)&g[0];


	msgFactory->WriteMessage(handShakeMsg);
}


/* void WebSocket::SendToClient(const boost::system::error_code& error, size_t bytes_transferred)
* @param 1: Boost system error code
* @param 2: Bytes transferred
* Note: Apart from sending data to client, the data frame is produced according to draft-ietf-hybi-thewebsocketprotocol-15
*/
void WebSocket::SendToClient(char *msgToSend)
{
	string m_TxBuffer;
	m_TxBuffer.append(msgToSend, strlen(msgToSend));
	cout << "SendToClient: " << msgToSend << "\n";
	//string::size_type index = m_TxBuffer.find("HTML5");

	//if (index != string::npos){

		//-- Masking Data
		//unsigned char *  m_TxBuffer_p = (unsigned char *)&m_TxBuffer[0];
		//MaskData(m_TxBuffer_p, m_TxBuffer.size());
		//-- End Masking Data

	WebSocketHelper *wsHelper = new WebSocketHelper();
	
	unsigned char* temp;
	wsHelper->WebSocketFraming(m_TxBuffer,temp);
	
	msgFactory->MsgSend(temp);
		//ToDo Ping
		/*
		try{
			if (m_ConnectionTimer.expires_from_now(boost::posix_time::seconds(ping_interval)) <= 0)
				m_ConnectionTimer.async_wait(boost::bind(&WebSocket::SendPing, this, boost::asio::placeholders::error));

			boost::asio::write(m_ClientSocket, boost::asio::buffer(buffer_to_send, len));
		}
		catch (boost::system::system_error& e)
		{
			//To Do close connection

			//ping to keep connection up
			//Sleep(reconnect_delay);

			//Accept Connection again(?)
			
		}
		*/
		m_TxBuffer.clear();
	//	}
}