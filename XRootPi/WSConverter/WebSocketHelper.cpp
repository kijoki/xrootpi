#include "WebSocketHelper.h"


WebSocketHelper::WebSocketHelper()
{
}


WebSocketHelper::~WebSocketHelper()
{
}

/*  int  WebSocketHelper::ParseMaskedData(unsigned char* key,unsigned char* data, unsigned char* actual_data)
* @param 1: Pointer to Key
* @param 2: Pointer to Masked Data
* @param 3: Pointer to Actual Data
* @return : Lenght of data
* Note: ParseMaskedData will Parse TCP data packet and extract websocket specified data sets.
*		 Also note that this function is done in a crude fashion.
*/
int WebSocketHelper::ParseMaskedData(unsigned char* key, unsigned char* data, unsigned char* actual_data)
{

	int len = data[1] - 0x80;
	unsigned char mask_key[4];
	if (len<126) {
		for (int i = 0; i<4; i++){
			key[i] = data[2 + i];
			mask_key[i] = key[i];
		}
		for (int i = 0; i<len; i++)
			actual_data[i] = data[6 + i];

	}
	else if (len == 126) {
		unsigned int len_2 = 0;
		len_2 = data[2];
		len_2 = len_2 << 8;
		len_2 = len_2 + data[3];

		for (int i = 0; i<4; i++){
			key[i] = data[4 + i];
			mask_key[i] = key[i];
		}
		for (int i = 0; i<(int)len_2; i++)
			actual_data[i] = data[8 + i];

		len = len_2;

	}
	else if (len == 127) {

	}

	return (int)len;
}

/* string WebSocketHelper::UnMaskData(unsigned char* key, unsigned char* data, int len)
* @param 1: key
* @param 2: masked data
* @param 3: data length
* @return : un masked data
*/
void WebSocketHelper::UnMaskData(unsigned char* key, unsigned char* data, int len, unsigned char* un_masked_data)
{
	int j = 0;
	//unsigned char un_masked_data[2048];
	for (int i = 0; i<len; i++) {
		j = i % 4;
		un_masked_data[i] = data[i] ^ key[j];
	}
	un_masked_data[len] = '\0';

	//return un_masked_data;
}

void WebSocketHelper::WebSocketFraming(string m_TxBuffer, unsigned char* buffer_to_send)
{
	//-- Start Websocket Framing
	//unsigned char buffer_to_send[4096];
	bool fin = 1; // is final frame?
	bool rsv_1 = 0; // Extension Related Must be Zero Unless Extended
	bool rsv_2 = 0; // Extension Related Must be Zero Unless Extended
	bool rsv_3 = 0; // Extension Related Must be Zero Unless Extended
	unsigned char opcode = 0x01; //Opcode leave it as 0x01, for text frame
	bool mask = 0; // Yes! unless client wants it masked
	unsigned int payload_len = 0;
	unsigned int m_TxBufferSize = m_TxBuffer.size();
	int len = 0;

	// Set Payload Length
	if (m_TxBuffer.size() <= 125)
		payload_len = m_TxBuffer.size();
	else if ((m_TxBuffer.size() <= 0x0000ffff) & (m_TxBuffer.size() >= 126))
		payload_len = 126;
	else
		payload_len = 127;

	//Do some calc...magic to put it in buffer
	buffer_to_send[0] = 0x00; // Do we need to do this? yes we do
	buffer_to_send[0] = buffer_to_send[0] | (fin << 7);
	buffer_to_send[0] = buffer_to_send[0] | (rsv_1 << 6);
	buffer_to_send[0] = buffer_to_send[0] | (rsv_2 << 5);
	buffer_to_send[0] = buffer_to_send[0] | (rsv_3 << 4);

	buffer_to_send[0] = buffer_to_send[0] | opcode;
	buffer_to_send[1] = 0x00;
	buffer_to_send[1] = buffer_to_send[1] | (mask << 7);
	buffer_to_send[1] = buffer_to_send[1] | (payload_len & 0xff);
	buffer_to_send[2] = 0x00; // Do we need to do this? yes we do

	// Two different buffer sizes allowed one less than 126 other more

	if (payload_len == 126){

		buffer_to_send[3] = m_TxBufferSize & 0xff;
		buffer_to_send[2] = (m_TxBufferSize & 0xff00) >> 8;
		/*if (isMask)
		{
			buffer_to_send[4] = mask_key[0];
			buffer_to_send[5] = mask_key[1];
			buffer_to_send[6] = mask_key[2];
			buffer_to_send[7] = mask_key[3];
		}*/

		for (int i = 0; i<(int)m_TxBuffer.size(); i++)
			buffer_to_send[i + 8] = m_TxBuffer[i];

		/* if (isMask)
			len = m_TxBuffer.size() + 8;
		else
		*/
			len = m_TxBuffer.size() + 4;

	}
	else {
		/* temp hack
		if (isMask)
		{
		buffer_to_send[2] = mask_key[0];
		buffer_to_send[3] = mask_key[1];
		buffer_to_send[4] = mask_key[2];
		buffer_to_send[5] = mask_key[3];
		}
		*/

		for (int i = 0; i<(int)m_TxBuffer.size(); i++)
			buffer_to_send[i + 2] = m_TxBuffer[i];

		/*	temp hack
		if (isMask)
		len = m_TxBuffer.size() + 6;
		else */
		len = m_TxBuffer.size() + 2;

	}
	buffer_to_send[len] = '\0';
	//return &buffer_to_send[0];
	//-- End Websocket Framing
}