#ifndef WEBSOCKETCONVERTER_H
#define WEBSOCKETCONVERTER_H

#include <iostream>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>
#include "../ASyncTCP//TCPClient.h"
#include "../WebSocket//WebSocket.h"

class WSConverter
{
public:
	WSConverter(char *ip, int s_port, int ws_port);
	~WSConverter();
	char* ws_ip;
	int ws_port;
	char* server_ip;
	int server_port;
	void ReadConfigFile();
	void WSCallback(unsigned char* y);
	void ClientCallback(unsigned char* y);
	MessageFactory *ws_msgf;
	MessageFactory *server_msgf;
	TCPServer *ts;
	WebSocket *ws;
};

#endif

