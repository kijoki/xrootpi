#include "WSConverter.h"


WSConverter::WSConverter(char *ip, int s_port, int w_port)
{
	//ReadConfigFile();
	boost::asio::io_service mIO;

	ws_msgf = new MessageFactory();
	ws_msgf->MsgRecv.connect(boost::bind(&WSConverter::WSCallback, this, _1));

	server_msgf = new MessageFactory();
	server_msgf->MsgRecv.connect(boost::bind(&WSConverter::ClientCallback, this, _1));


	ts = new TCPServer(ip, s_port, mIO, server_msgf);

	ws = new WebSocket(ip, w_port, mIO, ws_msgf);
	  
	
}


WSConverter::~WSConverter()
{
}
//void WSConverter::operator()(unsigned char* y) const { std::cout << y; }

void WSConverter::WSCallback(unsigned char* y)
{

	if (y[0] == 129)
	{
		
		unsigned char data[1024];
		unsigned char *data_p = &data[0];
		unsigned char key[4];
		unsigned char *key_p = &key[0];
		//Parsing
		WebSocketHelper *WSHelper = new WebSocketHelper();
		int len = WSHelper->ParseMaskedData(key_p, y, data_p);
		//unmasking
		unsigned char *temp = new unsigned char[len];
		temp = WSHelper->UnMaskData(key_p, data_p, len);
		
		std::stringstream ss;
		ss << temp;
		
		std::string g = ss.str();

		unsigned char* msg = (unsigned char*)&g[0];

		std::cout << "READ Completed\n";
		ts->Broadcast(msg);
	}
}

void WSConverter::ClientCallback(unsigned char* y)
{

	if (y[0] != '\r')
	{
		std::string m_TxBuffer;
		m_TxBuffer.append((char*)y, std::strlen((char*)y));
		std::cout << "SendToClient: " << y << "\n";

		WebSocketHelper *wsHelper = new WebSocketHelper();
		unsigned char* temp = wsHelper->WebSocketFraming(m_TxBuffer);
		std::stringstream ss;
		ss << temp;

		std::string g = ss.str();

		unsigned char* msg = (unsigned char*)&g[0];
		ws_msgf->MsgSend(msg);
	}
}

/* WSConverter::ReadConfigFile()
* Note: Reads Config File.. This is a temporary measure.
*/
void WSConverter::ReadConfigFile()
{
	try
	{
		std::string filename = "WSConverter.cfg";
		boost::property_tree::ptree pt;
		boost::property_tree::read_xml(filename, pt);

		BOOST_FOREACH(boost::property_tree::ptree::value_type const& v, pt.get_child("WSConverter")) {
			if (v.first == "WSConfig") {
				ws_ip = v.second.get<char*>("addr");
				ws_port = v.second.get<int>("port");
			}
			else if (v.first == "TCPServer"){
				server_ip = v.second.get<char*>("addr");
				server_port = v.second.get<int>("port");
			}
		}
	}
	catch (boost::system::system_error& e){
		std::cout << "Reading Config File error : " << e.code().message();
	}
}