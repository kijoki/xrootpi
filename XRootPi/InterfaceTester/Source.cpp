
#include <iostream>
#include "../ASyncTCP/TCPServer.h"
#include "../ASyncTCP/TCPClient.h"
#include "../WebSocket/WebSocket.h"
#include <boost/thread.hpp>


struct print {
	void operator()(unsigned char* y) const { std::cout << y; }
};
int main(int argc, char* argv[])
{
	boost::asio::io_service mIO;

	MessageFactory *msgFactory = new MessageFactory();
	msgFactory->MsgRecv.connect(print());

	if (argc < 4)
	{
		cout << "Usage exe 1{TCPServer or TCPClient or WSServer} 2{IP} 3{port}";

		return 0;
	}
	char *ip = argv[2];

	int port = atoi(argv[3]);

	string type = string(argv[1]);
/*
	if (type == "TCPServer")
	{
	
		TCPServer s(ip, port, mIO, msgFactory);
		
	}
	else if (type == "TCPClient")
	{
		
		TCPClient ts(ip, port, mIO, msgFactory);
		
	} 
	else if (type == "WSServer")
	{
	
		WebSocket ws(ip, port, mIO, msgFactory);
	} */
	TCPClient ws(ip, port, mIO, msgFactory);

	/*WebSocket Server*/
	//WebSocket s;
	//s.Run();

	/*TCP Client*/
	

	
	//TCPServer s("0.0.0.0", 8056, mIO, msgFactory);
	/*TCP Client*/
	//TCPClient ts("127.0.0.1", 8056, mIO, msgFactory);
	/*TCP Server*/
	//WebSocket ts("127.0.0.1", 65, mIO, msgFactory);
	//ts.Run();

	boost::thread t(boost::bind(static_cast<size_t(boost::asio::io_service::*)()>(&boost::asio::io_service::run), &mIO));
	//m_Buffer[bytes_transferred] = 0;
	//std::cout << bytes_transferred << "message: " << m_Buffer << std::endl;

	
	char *line = new char[100];
	while (std::cin.getline(line, 100))
	{
		msgFactory->WriteMessage((unsigned char *)line);
	}
	
	t.join();

	return 0;

}
